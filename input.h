//
// Created by nedelko on 7/19/16.
//

#ifndef LAB7_INPUT_H
#define LAB7_INPUT_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int digit_to_int(char);

int input(int ***, char *, int, int *, int *);

#endif //LAB7_INPUT_H
