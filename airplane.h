//
// CREATED BY NEDELKO ON 7/19/16.
//

#ifndef LAB7_AIRPLANE_H
#define LAB7_AIRPLANE_H

#include <stdio.h>

int direction(int *, int);

int airplane(int **, int *, int);

#endif //LAB7_AIRPLANE_H
