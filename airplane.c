//
// Created by nedelko on 7/19/16.
//

#include "airplane.h"

int direction(int *mas, int w) {
    if (mas[0] < w / 2) return 1;
    return 0;
}

int airplane(int **map, int *coord, int w) {
    int targets=0, start, end;
    if (direction(coord, w)) {
        start = coord[0];
        end = w;
    }
    else {
        start = 0;
        end = coord[0];
    }
    for (int i = start; i < end; ++i) {
        targets += map[coord[1]][i];
    }
    return targets;
}