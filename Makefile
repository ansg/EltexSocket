socket : tcp_client.c tcp_server.c airplane.c input.c
	gcc tcp_client.c -o client -std=c99 -Wall
	gcc tcp_server.c airplane.c input.c -o server -std=c99 -Wall
thread : DieWithError.c TCPEchoClient.c
	gcc -o TCPEchoClient TCPEchoClient.c DieWithError.c
	gcc -o TCPEchoServer-Thread TCPEchoServer-Thread.c DieWithError.c AcceptTCPConnection.c CreateTCPServerSocket.c HandleTCPClient.c airplane.c input.c -pthread -std=c99
clean :
	rm client server
