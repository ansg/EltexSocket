//
// Created by nedelko on 7/19/16.
//

#include "input.h"

int digit_to_int(char d) {
    char str[2];
    str[0] = d;
    str[1] = '\0';
    return (int) strtol(str, NULL, 10);
}

int input(int ***map, char *source, int sc, int *h, int *w) {
    char buf;
    int handle;
    //*sc = digit_to_int(s);
    handle = open(source, O_RDONLY);
    if (!handle) {
        printf("File not found %s!\n", source);
        exit(-2);
    }
    read(handle, &buf, 1);
    *h = digit_to_int(buf);
    read(handle, &buf, 1);
    *w = digit_to_int(buf);
    if (sc >= *h) {
        printf("to much airplane! h = %d \n", *h);
        //return 1;
    }
    *map = (int **) malloc(sizeof(int *) * (*h));
    for (int i = 0; i < (*h); ++i) {
        (*map)[i] = (int *) malloc(sizeof(int) * (*w));
        for (int j = 0; j < (*w); ++j) {
            read(handle, &buf, 1);
            if (buf == '\n') {
                j--;
                continue;
            }
            (*map)[i][j] = digit_to_int(buf);
            //printf("%c\n", buf);
        }
    }
    return 0;
}